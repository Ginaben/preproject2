package com.example.preProject.entity.project;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@Inheritance(strategy = InheritanceType.JOINED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_id")
    private Long id;

    //고객사
    private String clientName;

    //담당자 이름
    private String picName;

    //담당자 연락처
    private String picContact;

    //담당자 메일
    private String picMail;

    //담당자 직급
    private String picPosition;

    //담장자 소속부서
    private String picTeam;

    //담당자 업무
    private String Duty;

    //고객사 사업자증명서


}
