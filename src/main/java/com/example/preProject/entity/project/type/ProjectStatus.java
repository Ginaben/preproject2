package com.example.preProject.entity.project.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ProjectStatus {

    //사업예정, 계약예정, 계약대기, 계약진행중 -> ??
    CONTRACTED("계약완료"), CANCELED("사업중단"), COMPLETED("사업완료");

    private final String projectStatus;
}
