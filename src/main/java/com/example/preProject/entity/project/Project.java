package com.example.preProject.entity.project;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Getter
@Inheritance(strategy = InheritanceType.JOINED)
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ToString
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "project_id")
    private Long id;

    //사업명
    private String projectName;

    //근무지
    private String location;

    //필요인력
    private int personnel;

    //프로젝트 시작 기간
    //프로젝트 마감 기간

    //비고

}
