package com.example.preProject.entity.project.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ProjectType {

    // 인력투입은 뭘로하지
    SM("SM"), SI("SI"), SOLUTION("SOLUTION");

    private final String projectType;
}
