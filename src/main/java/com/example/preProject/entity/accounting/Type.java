package com.example.preProject.entity.accounting;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Type {

    MONTHLY("월발행"), ADVANCE("선금"), SECOND("중도금"), BALANCE("잔금"), FULL("전액");

    private final String type;


}
