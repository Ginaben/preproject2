package com.example.preProject.entity.employee.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum SkillGrade {

    SUPERIOR("특급"), ADVANCED("고급"), HIGH_INTERMEDIATE("중급이상"),
    INTERMEDIATE("중급"), LOW_INTERMEDIATE("중급이하"), NOVICE("초급");

    private final String skillGrade;

}
