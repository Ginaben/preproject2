package com.example.preProject.entity.employee.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Type {

    FULL_TIME("정규직"), FREELANCER("직계약"), PARTNER("협력체");

    private final String type;

}
