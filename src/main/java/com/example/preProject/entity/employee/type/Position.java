package com.example.preProject.entity.employee.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Position {

    CEO("대표"), COO("상무"), DIRECTOR("이사"), MANAGER("부서장"), ASSISTANT_MANAGER("차장"),
    SUPERVISOR("과장"), ASSISTANT_SUPERVISOR("대리"), STAFF("사원");

    private final String position;

}
