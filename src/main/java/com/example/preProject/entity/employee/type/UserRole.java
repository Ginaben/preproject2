package com.example.preProject.entity.employee.type;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum UserRole {

    WRITER("WRITER"), READER("READER"), ADMIN("ADMIN"), NON_AUTH("NON_AUTH");

    private final String key;
}
